# ------------------------------------------------
# Developer Eun Im
# Date : 2020.Jul.10
# version: 0.1.0 beta
# Function
# :Samsung vcf files converts to google contacts...
# ------------------------------------------------
# Input: *.vcf on "./contacts"
# Output: Anycall_contacts.csv
# ------------------------------------------------
# Additional
# myIncoding.py
# myHex2Korean.py
# ------------------------------------------------

import myIncoding as mKorName
import os


path = "./contacts"
file_list = os.listdir(path)
file_list_vcf = [file for file in file_list if file.endswith(".vcf")]


for index, value in enumerate(file_list_vcf):
    strPhoneNumber = ""
    strName = ""
    strNameDumy = ""
    strPrefixPhone = "TEL;"
    strPrefixName = "FN;"

    #read file
    fullPathName = path + "/" +value
    f = open(fullPathName, 'r')
    while True:
        line = f.readline()
        if not line: break
        strTemp = ""

        if "TEL;" in line:
            strTemp = line
            nStart = strTemp.find(":")
            nEnd = strTemp.find("\n")
            strPhoneNumber = strTemp[nStart + 1:nEnd]


        if strPrefixName in line:
            strTemp = line
            nStart = strTemp.find(":")
            nEnd = strTemp.find("\n")
            strNameDumy = strTemp[nStart + 1:nEnd]
            strName = mKorName.MakingKoreanNameFromHex(strTemp)
            print(index, value, strName, strPhoneNumber)

    f.close()

    #write file
    f2 = open("Anycall_contacts.csv", 'a')
    if index == 0:
        strHeader = "Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Language,Photo,Group Membership,Phone 1 - Type,Phone 1 - Value \n"
        f2.write(strHeader)
    stePrefix = ",,,,,,,,,,,,,,,,,,,,,,,,,,* myContacts,Mobile,"
    strLong = strName + "," + strName + ","+stePrefix + strPhoneNumber + "\n"
    f2.write(strLong)
    f2.close()
