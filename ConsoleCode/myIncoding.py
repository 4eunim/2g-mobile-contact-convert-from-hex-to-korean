# ------------------------------------------------
# Developer Eun Im
# Date : 2020.Jul.10
# version: 1.0.0
# Function
# :read string and convert to as one contactName
# ------------------------------------------------
# test sample:
# input: FN;ENCODING=QUOTED-PRINTABLE:=BD=B0=C5=CD
# output: 쉼터
# ------------------------------------------------
# Example
# 대문자 16진수 : 선두 바이트
# 소문자 16진수 : 후미 바이트
# Ex) 가 = [B0, a1]
# ------------------------------------------------

import myHex2Korean as cKor


def MakingKoreanNameFromHex(x) :
    strContactNameTemp = x.rstrip('\n')
    nStart = strContactNameTemp.find(":")
    nEnd = len(strContactNameTemp)
    strContactName = strContactNameTemp[nStart + 1:nEnd]
    strContactName = strContactName.lower()

    strContactName_split = strContactName.split('=')
    strContactName = strContactName.replace("=", ' ')
    HowMayList = len(strContactName_split)

    strContactNameKor = ""

    for i in range(1, HowMayList, 2):
        strContactNameKorTemp = cKor.searchKoreanFromHex(strContactName_split[i], strContactName_split[i + 1])
        strContactNameKor = strContactNameKor + strContactNameKorTemp

    return strContactNameKor;

# ------------------------------------------------
# Test -------------------------------------------
#x = input('Input x : ')
#print(MakingKoreanNameFromHex(x))
# ------------------------------------------------