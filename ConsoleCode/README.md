## English (한국어 밑에보세요)
Target: Back up from Contact lists on 2G mobile (Samsung Anycall) to Google Account format *.csv
From file format    : *.vcf : Samsung Anycall
To file format      : *.csv : To upgrade Google Account as one file


####TEST Result my site:
This software convert from 680 files with *.vcf format to one *.csv file.
I hope to support your parents or family or friend or neighbor when they met a problem to back up.
There is no Korean Character on *.vcf. I found that two combinations with Hex convert to One Korean Character. I guess it’s “EUC-KR”
And I didn't found other character to matching with two hex. so Some characters are missing on DB.

####Example
Capital Hexadecimal : front byte # B0
small letter Hexadecimal : backside byte #a1
 Ex) 가 = [B0, a1] = // on software [b0, a1]


####How to use
1.	Put your all vcf format files under “contacts”
2.	To delete “Anycall_contacts.csv”
3.	Run “myConvertVcf2Csv.py”
4.	See description “Anycall_contacts.csv” and import this file on google/Contacts.

## 한국어
삼성 2G 애니콜 시리즈의 주소록 파일(*.vcf)를 구글 주소록에 넣도록 변환하는 소프트웨어 입니다.
삼성 애니콜 파일 포멧 : *.vcf 
구글 주소록에 들어갈 파일 포멧 : *.csv 

####테스트 결과:
680개 *.vcf 포멧 파일을 *.csv 파일 1개로 만들어 냈습니다.
저희 부모님을 위해 만든 건데 주변 지인, 가족, 친구 분들께 도움이 되면 좋겠습니다.
vcf 파일 보시면 한글이 없고 제 추측에 2개 Hex를 한국어 글자 1개로 표현합니다.
정확한 표현법은 모르나 추측은 “EUC-KR”이라 생각됩니다.
관련 DB를 찾지 못해서 일부 글자가 빠져있습니다.


####Example
대문자 16진수 : 앞쪽 바이트 # B0
소문자 16진수: 뒤쪽바이트 #a1
 Ex) 가 = [B0, a1] = // on software [b0, a1]


####How to use
1.	"contacts”폴더에 vcf확장명의 주소록을 넣어주세요.
2.	"Anycall_contacts.csv”파일을 삭제해주세요. (아직 소스에서 핸들링을 안했어요.)
3.	"myConvertVcf2Csv.py”실행
4.	생성된" Anycall_contacts.csv”파일 내용 확인하시고 구글 주소에 삽입해주시면됩니다.